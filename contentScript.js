/*
const uebernehmen = document.createElement("button")
uebernehmen.innerText = "Übernehmen"
uebernehmen.style.cssText = bootstrapButtonStyle
uebernehmen.onclick = function () {

	document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs > div > div:nth-child(2)").click()

	const postfach_code = document.querySelector("#grd-el-20").value

	document.querySelector("#grd-el-20").value = ""

	document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs > div > div:nth-child(1)").click()


	const txtar = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div.w-full.mt-4 > div > div.input-box.pl-2.mt-1.flex.multiline > textarea")

	const name = document.querySelector("#app > div.flex.h-screen > div.nav-open > nav > div > div.self-start.w-full.mb-10 > div > div.flex.cursor-pointer > div > div").innerText

	let lorem = `${postfach_code}, ${name}:
Alle AZ erklärt.AZ zusammengefasst: 0
AZ fallengelassen: 0
AZ neu beantragt: 0
Anteile Flächen nicht erklärt: 0
Informationen aus Telefonaten und Mail: -
Sonstige Bemerkungen:\n\n`


	txtar.value = lorem + txtar.value

	const lines = txtar.value.split("\n")
	txtar.setAttribute("rows", lines.length)

	document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(8) > div:nth-child(2) > div.dropdown > form > div > div.p-dropdown-trigger").click()

	document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(8) > div:nth-child(2) > div.dropdown > form > div > div.p-dropdown-panel.p-component > div.p-dropdown-items-wrapper > ul")

}*/
// BasisBisProtokoll.insertAdjacentElement('beforeend', uebernehmen)




const bootstrapButtonStyle = `display: inline-block;
font-weight: 400;
text-align: center;
white-space: nowrap;
vertical-align: middle;
user-select: none;
border: 1px solid transparent;
padding: .375rem .75rem;
font-size: 1rem;
line-height: 1.5;
border-radius: .25rem;
transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
color: #fff;
background-color: #007bff;
border-color: #007bff;`




function main() {
    let StandardEmpfangsvollmacht = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(8) > div:nth-child(1)")

    if (StandardEmpfangsvollmacht && StandardEmpfangsvollmacht.style.display != "none") {
		

        /*
        	Get data, Anschrift and Email
        */

        // jump to Kontakt
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs > div > div:nth-child(2)").click();


		let strasseHausnr, plzOrt, email, mobilnr, telefonnr
		try {
			strasseHausnr = (document.querySelector("#grd-el-18") ? document.querySelector("#grd-el-18").value : document.querySelector("#grd-el-82").value) + ' ' + (document.querySelector("#grd-el-19") ? document.querySelector("#grd-el-19").value : document.querySelector("#grd-el-83").value);

			plzOrt = (document.querySelector("#grd-el-21") ? document.querySelector("#grd-el-21").value : document.querySelector("#grd-el-85").value) + ' ' + (document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > div.dropdown > span > input") ? document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > div.dropdown > span > input").value : document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > div.dropdown > span > input").value);

			email = document.querySelector("#grd-el-22") ? document.querySelector("#grd-el-22").value : document.querySelector("#grd-el-86").value;

			mobilnr = document.querySelector("#grd-el-23") ? document.querySelector("#grd-el-23").value : (document.querySelector("#grd-el-87") ? document.querySelector("#grd-el-87").value : '-')

			telefonnr = document.querySelector("#grd-el-24") ? document.querySelector("#grd-el-24").value : (document.querySelector("#grd-el-88") ? document.querySelector("#grd-el-88").value : '-')
		} catch(e) {
			location.reload()
		}

        // jump back to Basis
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs > div > div:nth-child(1)").click();


        // end

		let anrede, nachname, vorname
		try {
			anrede = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div.dropdown > form > div > span").innerText

			nachname = document.querySelector("#grd-el-10") ? document.querySelector("#grd-el-10").value : document.querySelector("#grd-el-74").value
			vorname = document.querySelector("#grd-el-9") ? document.querySelector("#grd-el-9").value : document.querySelector("#grd-el-73").value
		} catch(e) {
			location.reload()
		}
		

		let anschrift = anrede + ' ' + vorname + ' ' + nachname + '\n'
		anschrift += strasseHausnr + '\n'
		anschrift += plzOrt


        // add watermark
		if(!document.getElementById("redesign")) {
			const RedesignActivated = document.createElement("code");
			RedesignActivated.style.userSelect = "none";
			RedesignActivated.id = "redesign"
			RedesignActivated.appendChild(document.createTextNode("Redesign applied!"));
			document.querySelector("#app > div.flex.h-screen > div.nav-open > nav > div > div:nth-child(1)").insertAdjacentElement('afterend', RedesignActivated)
		}


		// amend zentrale mandanten nr
		document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div > div:nth-child(1) > div > label").textContent += ' ' + nachname + ' ' + vorname


        /*
        	Append buttons
        */

        // get tab list
        const BasisBisProtokoll = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs > div")


	

        // Button Grundbuchabfrage
        const button_Grundbuchabfrage = document.createElement("button")
        button_Grundbuchabfrage.innerText = "Grundbuchabfrage"
        button_Grundbuchabfrage.style.cssText = bootstrapButtonStyle
        button_Grundbuchabfrage.onclick = function () {
            const nach_vorname_gbdatum = nachname + ", " + vorname + "%0D%0A" + document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(5) > div:nth-child(1) > div:nth-child(2) > div > span > input").value
			
            window.location.href = `mailto:Julia.Sauer@parta.de?subject=Bitte um Grundbuchabfrage - ${nachname}, ${vorname}&body=` + nach_vorname_gbdatum;
        }
        BasisBisProtokoll.insertAdjacentElement('beforeend', button_Grundbuchabfrage)

	



		// Button Email: LuF später
        const button_Email = document.createElement("button")
        button_Email.innerText = "Email: LuF später"
        button_Email.style.cssText = bootstrapButtonStyle
        button_Email.onclick = function () {
            const anrede = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div.dropdown > form > div > span").innerText
            const nachname = document.querySelector("#grd-el-10").value


            const emailtext = `Sehr geehrte/r ${anrede} ${nachname},
            %0D%0A %0D%0A
			anliegend erhalten Sie die nach Ihren Angaben und den uns vorliegenden Daten erstellten Grundsteuerfeststellungserklärung(en).
            %0D%0A %0D%0A
			Die Erklärungen für land- und forstwirtschaftliches Vermögen werden Ihnen zu einem späteren Zeitpunkt zugestellt und separat abgerechnet.
			%0D%0A %0D%0A
			Wir bitten Sie, diese auf Richtigkeit und Vollständigkeit zu prüfen und uns ggfs. innerhalb einer Frist von 10 Werktagen über notwendige Änderungen oder Ergänzungen zu informieren. Diese Daten werden wir nach Ablauf der 10 Werktage elektronisch an das jeweils zuständige Finanzamt übermitteln. Eine aktive Zustimmung Ihrerseits ist nicht erforderlich.
			%0D%0A %0D%0A
			Nach Übermittlung der Erklärung(en) an die Finanzbehörde, erhalten Sie unsere Gebührenabrechnung.
			%0D%0A %0D%0A
			Aufgrund der erteilten Vollmacht wird die Finanzverwaltung uns Ihre Grundsteuerbescheide zustellen. Wir werden diese auf Richtigkeit prüfen und Ihnen anschließend zusenden. Werden Ihnen versehentlich die Steuerbescheide zur Grundsteuer zugestellt, bitten wir um zeitnahe Zusendung zwecks Prüfung, damit wir ggf. innerhalb der Rechtsmittelfrist von einem Monat Einspruch einlegen können.
			%0D%0A %0D%0A
			Die ersten Feststellungsbescheide für land- und forstwirtschaftliches Vermögen erwarten wir frühestens im zweiten Quartal 2023.`



            window.location.href = `mailto:${email}?subject=Erklärung zur Feststellung des Grundsteuerwerts auf den 01.01.2022&body=${emailtext}`;
        }
        BasisBisProtokoll.insertAdjacentElement('beforeend', button_Email)

		// Button Email: LuF dabei
        const button_Email2 = document.createElement("button")
        button_Email2.innerText = "Email: LuF dabei"
        button_Email2.style.cssText = bootstrapButtonStyle
        button_Email2.onclick = function () {
            const anrede = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div.dropdown > form > div > span").innerText
            const nachname = document.querySelector("#grd-el-10").value


            const emailtext = `Sehr geehrte/r ${anrede} ${nachname},
            %0D%0A %0D%0A
			anliegend erhalten Sie die nach Ihren Angaben und den uns vorliegenden Daten erstellten Grundsteuerfeststellungserklärung(en).
            %0D%0A %0D%0A
			Wir bitten Sie, diese auf Richtigkeit und Vollständigkeit zu prüfen und uns ggfs. innerhalb einer Frist von 10 Werktagen über notwendige Änderungen oder Ergänzungen zu informieren. Diese Daten werden wir nach Ablauf der 10 Werktage elektronisch an das jeweils zuständige Finanzamt übermitteln. Eine aktive Zustimmung Ihrerseits ist nicht erforderlich.
			%0D%0A %0D%0A
			Nach Übermittlung der Erklärung(en) an die Finanzbehörde, erhalten Sie unsere Gebührenabrechnung.
			%0D%0A %0D%0A
			Aufgrund der erteilten Vollmacht wird die Finanzverwaltung uns Ihre Grundsteuerbescheide zustellen. Wir werden diese auf Richtigkeit prüfen und Ihnen anschließend zusenden. Werden Ihnen versehentlich die Steuerbescheide zur Grundsteuer zugestellt, bitten wir um zeitnahe Zusendung zwecks Prüfung, damit wir ggf. innerhalb der Rechtsmittelfrist von einem Monat Einspruch einlegen können.
			%0D%0A %0D%0A
			Die ersten Feststellungsbescheide für land- und forstwirtschaftliches Vermögen erwarten wir frühestens im zweiten Quartal 2023.`



            window.location.href = `mailto:${email}?subject=Erklärung zur Feststellung des Grundsteuerwerts auf den 01.01.2022&body=${emailtext}`;
        }
        BasisBisProtokoll.insertAdjacentElement('beforeend', button_Email2)


		/*
			Textareas for Kontakt data
		*/

		const mydiv = document.createElement("div")
		mydiv.classList.add("flex")
		

		const mobilnrTextarea = document.createElement("textarea"); 
		mobilnrTextarea.readOnly = true;
		mobilnrTextarea.value= "Mobilnr.:\n" + mobilnr
		mobilnrTextarea.style.overflow = "hidden"
		mobilnrTextarea.style.resize = "none"
        mobilnrTextarea.setAttribute("rows", 2)
		mobilnrTextarea.wrap = "off"
		mydiv.appendChild(mobilnrTextarea)
		
		const telefonnrTextarea = document.createElement("textarea"); 
		telefonnrTextarea.readOnly = true;
		telefonnrTextarea.value= "Telefonnr.:\n" + telefonnr
		telefonnrTextarea.style.overflow = "hidden"
		telefonnrTextarea.style.resize = "none"
        telefonnrTextarea.setAttribute("rows", 2)
		telefonnrTextarea.wrap = "off"
		mydiv.appendChild(telefonnrTextarea)

		const anschriftTextarea = document.createElement("textarea"); 
		anschriftTextarea.readOnly = true;
		anschriftTextarea.value= anschrift
		anschriftTextarea.style.overflow = "hidden"
		anschriftTextarea.style.resize = "none"
        anschriftTextarea.setAttribute("rows", 3)
		anschriftTextarea.wrap = "off"
		mydiv.appendChild(anschriftTextarea)

		document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tabs").insertAdjacentElement('afterend', mydiv)




        /*
        	apply design
        */

	
        const stl = document.createElement("style");
        stl.innerText = ".mt-4 { margin-top: 0.5rem; } .tab-content[data-v-73c2600d] { padding-top: 20px; }";
        document.querySelector("body").appendChild(stl);

        // Mandanten > Bearbeiten
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div.flex.flex-col.mb-1.relative.sticky-header > div.flex.justify-start.items-center.mt-2").style.display = "none"

        // Mandant bearbeiten
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div.flex.flex-col.mb-1.relative.sticky-header > div.flex.justify-between.items-center.mt-3.mr-6.page-caption").style.display = "none"

        // Support
        document.querySelector("#atlwdg-trigger").style.display = "none"

        // w-1/2 Anrede Titel
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)").className += " w-1/2"

        // w-1/2 Vorname Nachname
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(3)").className += " w-1/2"

        // w-1/2 Steuernummer Identifikationsnummer
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(4)").className += " w-1/2"

        // w-1/2 Geburtsdatum Vertreter
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(5)").className += " w-1/2"

        // w-1/2 Zuständig
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(8)").className += " w-1/2"

        let zst = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(8)")

        zst.className = zst.className.replace("space-x-8", "")

        // w-1/2 Zentrale Berater
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div").className += " w-1/2"

        // Mandatentyp
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2)").style.display = "none"

        // display none Organisation
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div.mt-8.flex.flex-col").style.display = "none"

        // display none Mandantenstatus | Niederlassung
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div:nth-child(7)").style.display = "none"

        // display none StandardEmpfangsvollmacht
        StandardEmpfangsvollmacht.style.display = "none"

        // textarea
        document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div.w-full.mt-4 > div").className += " w-1/2"

        const txtar = document.querySelector("#app > div.flex.h-screen > div.flex.flex-col.overflow-x-hidden.container-main.flex-auto > form > div > div:nth-child(8) > div.mt-4.mr-6.pb-8 > div.tab-content.h-full > div:nth-child(1) > div:nth-child(2) > div.w-full.mt-4 > div > div.input-box.pl-2.mt-1.flex.multiline > textarea")

        txtar.style.overflow = "hidden"

        const lines = txtar.value.split("\n")
        txtar.setAttribute("rows", lines.length)

        txtar.addEventListener("input", function () {
            const lines = this.value.split("\n")
            txtar.setAttribute("rows", lines.length)
        })
    }

}

setInterval(main, 1000)